/*
 * TS 28.550 Performance Data Streaming Service
 *
 * OAS 3.0.1 specification of the Performance Data Streaming Service
 *
 * API version: 16.3.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ListOfStreamInfoToUpdateRequestType struct for ListOfStreamInfoToUpdateRequestType
type ListOfStreamInfoToUpdateRequestType struct {
	ListOfStreamInfoToUpdate *[]StreamInfoToUpdatePropertyType `json:"listOfStreamInfoToUpdate,omitempty"`
}

// NewListOfStreamInfoToUpdateRequestType instantiates a new ListOfStreamInfoToUpdateRequestType object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewListOfStreamInfoToUpdateRequestType() *ListOfStreamInfoToUpdateRequestType {
	this := ListOfStreamInfoToUpdateRequestType{}
	return &this
}

// NewListOfStreamInfoToUpdateRequestTypeWithDefaults instantiates a new ListOfStreamInfoToUpdateRequestType object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewListOfStreamInfoToUpdateRequestTypeWithDefaults() *ListOfStreamInfoToUpdateRequestType {
	this := ListOfStreamInfoToUpdateRequestType{}
	return &this
}

// GetListOfStreamInfoToUpdate returns the ListOfStreamInfoToUpdate field value if set, zero value otherwise.
func (o *ListOfStreamInfoToUpdateRequestType) GetListOfStreamInfoToUpdate() []StreamInfoToUpdatePropertyType {
	if o == nil || o.ListOfStreamInfoToUpdate == nil {
		var ret []StreamInfoToUpdatePropertyType
		return ret
	}
	return *o.ListOfStreamInfoToUpdate
}

// GetListOfStreamInfoToUpdateOk returns a tuple with the ListOfStreamInfoToUpdate field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOfStreamInfoToUpdateRequestType) GetListOfStreamInfoToUpdateOk() (*[]StreamInfoToUpdatePropertyType, bool) {
	if o == nil || o.ListOfStreamInfoToUpdate == nil {
		return nil, false
	}
	return o.ListOfStreamInfoToUpdate, true
}

// HasListOfStreamInfoToUpdate returns a boolean if a field has been set.
func (o *ListOfStreamInfoToUpdateRequestType) HasListOfStreamInfoToUpdate() bool {
	if o != nil && o.ListOfStreamInfoToUpdate != nil {
		return true
	}

	return false
}

// SetListOfStreamInfoToUpdate gets a reference to the given []StreamInfoToUpdatePropertyType and assigns it to the ListOfStreamInfoToUpdate field.
func (o *ListOfStreamInfoToUpdateRequestType) SetListOfStreamInfoToUpdate(v []StreamInfoToUpdatePropertyType) {
	o.ListOfStreamInfoToUpdate = &v
}

func (o ListOfStreamInfoToUpdateRequestType) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.ListOfStreamInfoToUpdate != nil {
		toSerialize["listOfStreamInfoToUpdate"] = o.ListOfStreamInfoToUpdate
	}
	return json.Marshal(toSerialize)
}

type NullableListOfStreamInfoToUpdateRequestType struct {
	value *ListOfStreamInfoToUpdateRequestType
	isSet bool
}

func (v NullableListOfStreamInfoToUpdateRequestType) Get() *ListOfStreamInfoToUpdateRequestType {
	return v.value
}

func (v *NullableListOfStreamInfoToUpdateRequestType) Set(val *ListOfStreamInfoToUpdateRequestType) {
	v.value = val
	v.isSet = true
}

func (v NullableListOfStreamInfoToUpdateRequestType) IsSet() bool {
	return v.isSet
}

func (v *NullableListOfStreamInfoToUpdateRequestType) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableListOfStreamInfoToUpdateRequestType(val *ListOfStreamInfoToUpdateRequestType) *NullableListOfStreamInfoToUpdateRequestType {
	return &NullableListOfStreamInfoToUpdateRequestType{value: val, isSet: true}
}

func (v NullableListOfStreamInfoToUpdateRequestType) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableListOfStreamInfoToUpdateRequestType) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


