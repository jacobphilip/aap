# StreamInfoRetrievalResponseType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StreamInfoOut** | Pointer to [**StreamInfoOutType**](StreamInfoOutType.md) |  | [optional] 

## Methods

### NewStreamInfoRetrievalResponseType

`func NewStreamInfoRetrievalResponseType() *StreamInfoRetrievalResponseType`

NewStreamInfoRetrievalResponseType instantiates a new StreamInfoRetrievalResponseType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStreamInfoRetrievalResponseTypeWithDefaults

`func NewStreamInfoRetrievalResponseTypeWithDefaults() *StreamInfoRetrievalResponseType`

NewStreamInfoRetrievalResponseTypeWithDefaults instantiates a new StreamInfoRetrievalResponseType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreamInfoOut

`func (o *StreamInfoRetrievalResponseType) GetStreamInfoOut() StreamInfoOutType`

GetStreamInfoOut returns the StreamInfoOut field if non-nil, zero value otherwise.

### GetStreamInfoOutOk

`func (o *StreamInfoRetrievalResponseType) GetStreamInfoOutOk() (*StreamInfoOutType, bool)`

GetStreamInfoOutOk returns a tuple with the StreamInfoOut field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreamInfoOut

`func (o *StreamInfoRetrievalResponseType) SetStreamInfoOut(v StreamInfoOutType)`

SetStreamInfoOut sets StreamInfoOut field to given value.

### HasStreamInfoOut

`func (o *StreamInfoRetrievalResponseType) HasStreamInfoOut() bool`

HasStreamInfoOut returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


