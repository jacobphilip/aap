# StreamInfoOutType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StreamId** | Pointer to **string** |  | [optional] 
**IOCInstance** | Pointer to **string** |  | [optional] 
**MeasTypes** | Pointer to **[]string** |  | [optional] 

## Methods

### NewStreamInfoOutType

`func NewStreamInfoOutType() *StreamInfoOutType`

NewStreamInfoOutType instantiates a new StreamInfoOutType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStreamInfoOutTypeWithDefaults

`func NewStreamInfoOutTypeWithDefaults() *StreamInfoOutType`

NewStreamInfoOutTypeWithDefaults instantiates a new StreamInfoOutType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreamId

`func (o *StreamInfoOutType) GetStreamId() string`

GetStreamId returns the StreamId field if non-nil, zero value otherwise.

### GetStreamIdOk

`func (o *StreamInfoOutType) GetStreamIdOk() (*string, bool)`

GetStreamIdOk returns a tuple with the StreamId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreamId

`func (o *StreamInfoOutType) SetStreamId(v string)`

SetStreamId sets StreamId field to given value.

### HasStreamId

`func (o *StreamInfoOutType) HasStreamId() bool`

HasStreamId returns a boolean if a field has been set.

### GetIOCInstance

`func (o *StreamInfoOutType) GetIOCInstance() string`

GetIOCInstance returns the IOCInstance field if non-nil, zero value otherwise.

### GetIOCInstanceOk

`func (o *StreamInfoOutType) GetIOCInstanceOk() (*string, bool)`

GetIOCInstanceOk returns a tuple with the IOCInstance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIOCInstance

`func (o *StreamInfoOutType) SetIOCInstance(v string)`

SetIOCInstance sets IOCInstance field to given value.

### HasIOCInstance

`func (o *StreamInfoOutType) HasIOCInstance() bool`

HasIOCInstance returns a boolean if a field has been set.

### GetMeasTypes

`func (o *StreamInfoOutType) GetMeasTypes() []string`

GetMeasTypes returns the MeasTypes field if non-nil, zero value otherwise.

### GetMeasTypesOk

`func (o *StreamInfoOutType) GetMeasTypesOk() (*[]string, bool)`

GetMeasTypesOk returns a tuple with the MeasTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeasTypes

`func (o *StreamInfoOutType) SetMeasTypes(v []string)`

SetMeasTypes sets MeasTypes field to given value.

### HasMeasTypes

`func (o *StreamInfoOutType) HasMeasTypes() bool`

HasMeasTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


