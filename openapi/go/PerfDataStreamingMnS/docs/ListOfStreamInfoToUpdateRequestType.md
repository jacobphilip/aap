# ListOfStreamInfoToUpdateRequestType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ListOfStreamInfoToUpdate** | Pointer to [**[]StreamInfoToUpdatePropertyType**](StreamInfoToUpdatePropertyType.md) |  | [optional] 

## Methods

### NewListOfStreamInfoToUpdateRequestType

`func NewListOfStreamInfoToUpdateRequestType() *ListOfStreamInfoToUpdateRequestType`

NewListOfStreamInfoToUpdateRequestType instantiates a new ListOfStreamInfoToUpdateRequestType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListOfStreamInfoToUpdateRequestTypeWithDefaults

`func NewListOfStreamInfoToUpdateRequestTypeWithDefaults() *ListOfStreamInfoToUpdateRequestType`

NewListOfStreamInfoToUpdateRequestTypeWithDefaults instantiates a new ListOfStreamInfoToUpdateRequestType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetListOfStreamInfoToUpdate

`func (o *ListOfStreamInfoToUpdateRequestType) GetListOfStreamInfoToUpdate() []StreamInfoToUpdatePropertyType`

GetListOfStreamInfoToUpdate returns the ListOfStreamInfoToUpdate field if non-nil, zero value otherwise.

### GetListOfStreamInfoToUpdateOk

`func (o *ListOfStreamInfoToUpdateRequestType) GetListOfStreamInfoToUpdateOk() (*[]StreamInfoToUpdatePropertyType, bool)`

GetListOfStreamInfoToUpdateOk returns a tuple with the ListOfStreamInfoToUpdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetListOfStreamInfoToUpdate

`func (o *ListOfStreamInfoToUpdateRequestType) SetListOfStreamInfoToUpdate(v []StreamInfoToUpdatePropertyType)`

SetListOfStreamInfoToUpdate sets ListOfStreamInfoToUpdate field to given value.

### HasListOfStreamInfoToUpdate

`func (o *ListOfStreamInfoToUpdateRequestType) HasListOfStreamInfoToUpdate() bool`

HasListOfStreamInfoToUpdate returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


