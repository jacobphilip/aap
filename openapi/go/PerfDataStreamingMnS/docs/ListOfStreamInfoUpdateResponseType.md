# ListOfStreamInfoUpdateResponseType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ListOfStreamInfoUpdated** | Pointer to [**[]StreamInfoOutType**](StreamInfoOutType.md) |  | [optional] 

## Methods

### NewListOfStreamInfoUpdateResponseType

`func NewListOfStreamInfoUpdateResponseType() *ListOfStreamInfoUpdateResponseType`

NewListOfStreamInfoUpdateResponseType instantiates a new ListOfStreamInfoUpdateResponseType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListOfStreamInfoUpdateResponseTypeWithDefaults

`func NewListOfStreamInfoUpdateResponseTypeWithDefaults() *ListOfStreamInfoUpdateResponseType`

NewListOfStreamInfoUpdateResponseTypeWithDefaults instantiates a new ListOfStreamInfoUpdateResponseType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetListOfStreamInfoUpdated

`func (o *ListOfStreamInfoUpdateResponseType) GetListOfStreamInfoUpdated() []StreamInfoOutType`

GetListOfStreamInfoUpdated returns the ListOfStreamInfoUpdated field if non-nil, zero value otherwise.

### GetListOfStreamInfoUpdatedOk

`func (o *ListOfStreamInfoUpdateResponseType) GetListOfStreamInfoUpdatedOk() (*[]StreamInfoOutType, bool)`

GetListOfStreamInfoUpdatedOk returns a tuple with the ListOfStreamInfoUpdated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetListOfStreamInfoUpdated

`func (o *ListOfStreamInfoUpdateResponseType) SetListOfStreamInfoUpdated(v []StreamInfoOutType)`

SetListOfStreamInfoUpdated sets ListOfStreamInfoUpdated field to given value.

### HasListOfStreamInfoUpdated

`func (o *ListOfStreamInfoUpdateResponseType) HasListOfStreamInfoUpdated() bool`

HasListOfStreamInfoUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


