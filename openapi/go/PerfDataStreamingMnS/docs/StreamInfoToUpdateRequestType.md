# StreamInfoToUpdateRequestType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StreamInfoToUpdate** | Pointer to [**StreamInfoToUpdatePropertyType**](StreamInfoToUpdatePropertyType.md) |  | [optional] 

## Methods

### NewStreamInfoToUpdateRequestType

`func NewStreamInfoToUpdateRequestType() *StreamInfoToUpdateRequestType`

NewStreamInfoToUpdateRequestType instantiates a new StreamInfoToUpdateRequestType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStreamInfoToUpdateRequestTypeWithDefaults

`func NewStreamInfoToUpdateRequestTypeWithDefaults() *StreamInfoToUpdateRequestType`

NewStreamInfoToUpdateRequestTypeWithDefaults instantiates a new StreamInfoToUpdateRequestType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreamInfoToUpdate

`func (o *StreamInfoToUpdateRequestType) GetStreamInfoToUpdate() StreamInfoToUpdatePropertyType`

GetStreamInfoToUpdate returns the StreamInfoToUpdate field if non-nil, zero value otherwise.

### GetStreamInfoToUpdateOk

`func (o *StreamInfoToUpdateRequestType) GetStreamInfoToUpdateOk() (*StreamInfoToUpdatePropertyType, bool)`

GetStreamInfoToUpdateOk returns a tuple with the StreamInfoToUpdate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreamInfoToUpdate

`func (o *StreamInfoToUpdateRequestType) SetStreamInfoToUpdate(v StreamInfoToUpdatePropertyType)`

SetStreamInfoToUpdate sets StreamInfoToUpdate field to given value.

### HasStreamInfoToUpdate

`func (o *StreamInfoToUpdateRequestType) HasStreamInfoToUpdate() bool`

HasStreamInfoToUpdate returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


