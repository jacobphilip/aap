# StreamInfoToUpdatePropertyType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IOCInstance** | Pointer to **string** | The updated measured object instance, empty value means no update. | [optional] 
**MeasTypes** | Pointer to **[]string** | The updated list of measurement type, empty value means no update. | [optional] 

## Methods

### NewStreamInfoToUpdatePropertyType

`func NewStreamInfoToUpdatePropertyType() *StreamInfoToUpdatePropertyType`

NewStreamInfoToUpdatePropertyType instantiates a new StreamInfoToUpdatePropertyType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStreamInfoToUpdatePropertyTypeWithDefaults

`func NewStreamInfoToUpdatePropertyTypeWithDefaults() *StreamInfoToUpdatePropertyType`

NewStreamInfoToUpdatePropertyTypeWithDefaults instantiates a new StreamInfoToUpdatePropertyType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIOCInstance

`func (o *StreamInfoToUpdatePropertyType) GetIOCInstance() string`

GetIOCInstance returns the IOCInstance field if non-nil, zero value otherwise.

### GetIOCInstanceOk

`func (o *StreamInfoToUpdatePropertyType) GetIOCInstanceOk() (*string, bool)`

GetIOCInstanceOk returns a tuple with the IOCInstance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIOCInstance

`func (o *StreamInfoToUpdatePropertyType) SetIOCInstance(v string)`

SetIOCInstance sets IOCInstance field to given value.

### HasIOCInstance

`func (o *StreamInfoToUpdatePropertyType) HasIOCInstance() bool`

HasIOCInstance returns a boolean if a field has been set.

### GetMeasTypes

`func (o *StreamInfoToUpdatePropertyType) GetMeasTypes() []string`

GetMeasTypes returns the MeasTypes field if non-nil, zero value otherwise.

### GetMeasTypesOk

`func (o *StreamInfoToUpdatePropertyType) GetMeasTypesOk() (*[]string, bool)`

GetMeasTypesOk returns a tuple with the MeasTypes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeasTypes

`func (o *StreamInfoToUpdatePropertyType) SetMeasTypes(v []string)`

SetMeasTypes sets MeasTypes field to given value.

### HasMeasTypes

`func (o *StreamInfoToUpdatePropertyType) HasMeasTypes() bool`

HasMeasTypes returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


