# \DefaultApi

All URIs are relative to *http://example.com/PerfDataStreamMnS/v1630*

Method | HTTP request | Description
------------- | ------------- | -------------
[**StreamConnectionGet**](DefaultApi.md#StreamConnectionGet) | **Get** /streamConnection | The connection for streaming from the producer to the consumer
[**StreamInfoListDelete**](DefaultApi.md#StreamInfoListDelete) | **Delete** /streamInfoList | The information of streams to be deleted by the producer to the consumer
[**StreamInfoListGet**](DefaultApi.md#StreamInfoListGet) | **Get** /streamInfoList | Read resources of stream information from the streaming consumer
[**StreamInfoListPatch**](DefaultApi.md#StreamInfoListPatch) | **Patch** /streamInfoList | Update resources of stream information to the streaming consumer
[**StreamInfoListPost**](DefaultApi.md#StreamInfoListPost) | **Post** /streamInfoList | The set of information about the streams sent from the producer to the consumer
[**StreamInfoListStreamIdDelete**](DefaultApi.md#StreamInfoListStreamIdDelete) | **Delete** /streamInfoList/{streamId} | The stream information to be deleted by the producer to the consumer
[**StreamInfoListStreamIdGet**](DefaultApi.md#StreamInfoListStreamIdGet) | **Get** /streamInfoList/{streamId} | Read resource of the stream information from the streaming consumer
[**StreamInfoListStreamIdPatch**](DefaultApi.md#StreamInfoListStreamIdPatch) | **Patch** /streamInfoList/{streamId} | Update the resource of stream information to the streaming consumer



## StreamConnectionGet

> ErrorResponseType StreamConnectionGet(ctx).Upgrade(upgrade).Connection(connection).SecWebSocketKey(secWebSocketKey).SecWebSocketVersion(secWebSocketVersion).Execute()

The connection for streaming from the producer to the consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    upgrade := openapiclient.Upgrade-HeaderType("websocket") // UpgradeHeaderType | 
    connection := openapiclient.Connection-HeaderType("Upgrade") // ConnectionHeaderType | 
    secWebSocketKey := "secWebSocketKey_example" // string | 
    secWebSocketVersion := "secWebSocketVersion_example" // string | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamConnectionGet(context.Background()).Upgrade(upgrade).Connection(connection).SecWebSocketKey(secWebSocketKey).SecWebSocketVersion(secWebSocketVersion).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamConnectionGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StreamConnectionGet`: ErrorResponseType
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.StreamConnectionGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiStreamConnectionGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **upgrade** | [**UpgradeHeaderType**](UpgradeHeaderType.md) |  | 
 **connection** | [**ConnectionHeaderType**](ConnectionHeaderType.md) |  | 
 **secWebSocketKey** | **string** |  | 
 **secWebSocketVersion** | **string** |  | 

### Return type

[**ErrorResponseType**](ErrorResponseType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StreamInfoListDelete

> StreamInfoListDelete(ctx).StreamIdList(streamIdList).Execute()

The information of streams to be deleted by the producer to the consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    streamIdList := []int32{int32(123)} // []int32 | This parameter identifies the list of streamId to select from the collection resources identified with the path component of the URI.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamInfoListDelete(context.Background()).StreamIdList(streamIdList).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamInfoListDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiStreamInfoListDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **streamIdList** | **[]int32** | This parameter identifies the list of streamId to select from the collection resources identified with the path component of the URI. | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StreamInfoListGet

> ListOfStreamInfoRetrievalResponseType StreamInfoListGet(ctx).StreamIdList(streamIdList).Execute()

Read resources of stream information from the streaming consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    streamIdList := []int32{int32(123)} // []int32 | This parameter identifies the list of streamId to select from the collection resources identified with the path component of the URI.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamInfoListGet(context.Background()).StreamIdList(streamIdList).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamInfoListGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StreamInfoListGet`: ListOfStreamInfoRetrievalResponseType
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.StreamInfoListGet`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiStreamInfoListGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **streamIdList** | **[]int32** | This parameter identifies the list of streamId to select from the collection resources identified with the path component of the URI. | 

### Return type

[**ListOfStreamInfoRetrievalResponseType**](ListOfStreamInfoRetrievalResponseType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StreamInfoListPatch

> ListOfStreamInfoUpdateResponseType StreamInfoListPatch(ctx).StreamIdList(streamIdList).ListOfStreamInfoToUpdateRequestType(listOfStreamInfoToUpdateRequestType).Execute()

Update resources of stream information to the streaming consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    streamIdList := []int32{int32(123)} // []int32 | This parameter identifies the list of streamId to select from the collection resources identified with the path component of the URI.
    listOfStreamInfoToUpdateRequestType := *openapiclient.NewListOfStreamInfoToUpdateRequestType() // ListOfStreamInfoToUpdateRequestType | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamInfoListPatch(context.Background()).StreamIdList(streamIdList).ListOfStreamInfoToUpdateRequestType(listOfStreamInfoToUpdateRequestType).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamInfoListPatch``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StreamInfoListPatch`: ListOfStreamInfoUpdateResponseType
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.StreamInfoListPatch`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiStreamInfoListPatchRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **streamIdList** | **[]int32** | This parameter identifies the list of streamId to select from the collection resources identified with the path component of the URI. | 
 **listOfStreamInfoToUpdateRequestType** | [**ListOfStreamInfoToUpdateRequestType**](ListOfStreamInfoToUpdateRequestType.md) |  | 

### Return type

[**ListOfStreamInfoUpdateResponseType**](ListOfStreamInfoUpdateResponseType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StreamInfoListPost

> StreamInfoListPostResponseType StreamInfoListPost(ctx).StreamInfoListPostRequestType(streamInfoListPostRequestType).Execute()

The set of information about the streams sent from the producer to the consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    streamInfoListPostRequestType := *openapiclient.NewStreamInfoListPostRequestType() // StreamInfoListPostRequestType | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamInfoListPost(context.Background()).StreamInfoListPostRequestType(streamInfoListPostRequestType).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamInfoListPost``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StreamInfoListPost`: StreamInfoListPostResponseType
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.StreamInfoListPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiStreamInfoListPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **streamInfoListPostRequestType** | [**StreamInfoListPostRequestType**](StreamInfoListPostRequestType.md) |  | 

### Return type

[**StreamInfoListPostResponseType**](StreamInfoListPostResponseType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StreamInfoListStreamIdDelete

> StreamInfoListStreamIdDelete(ctx, streamId).Execute()

The stream information to be deleted by the producer to the consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    streamId := "streamId_example" // string | Identifies the stream for which the information is to be deleted

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamInfoListStreamIdDelete(context.Background(), streamId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamInfoListStreamIdDelete``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**streamId** | **string** | Identifies the stream for which the information is to be deleted | 

### Other Parameters

Other parameters are passed through a pointer to a apiStreamInfoListStreamIdDeleteRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StreamInfoListStreamIdGet

> ListOfStreamInfoRetrievalResponseType StreamInfoListStreamIdGet(ctx, streamId).Execute()

Read resource of the stream information from the streaming consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    streamId := "streamId_example" // string | Identifies the stream for which the information is to be retrieved.

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamInfoListStreamIdGet(context.Background(), streamId).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamInfoListStreamIdGet``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StreamInfoListStreamIdGet`: ListOfStreamInfoRetrievalResponseType
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.StreamInfoListStreamIdGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**streamId** | **string** | Identifies the stream for which the information is to be retrieved. | 

### Other Parameters

Other parameters are passed through a pointer to a apiStreamInfoListStreamIdGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ListOfStreamInfoRetrievalResponseType**](ListOfStreamInfoRetrievalResponseType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## StreamInfoListStreamIdPatch

> StreamInfoUpdateResponseType StreamInfoListStreamIdPatch(ctx, streamId).StreamInfoToUpdateRequestType(streamInfoToUpdateRequestType).Execute()

Update the resource of stream information to the streaming consumer



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    streamId := "streamId_example" // string | Identifies the stream for which the information is to be updated.
    streamInfoToUpdateRequestType := *openapiclient.NewStreamInfoToUpdateRequestType() // StreamInfoToUpdateRequestType | 

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.DefaultApi.StreamInfoListStreamIdPatch(context.Background(), streamId).StreamInfoToUpdateRequestType(streamInfoToUpdateRequestType).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `DefaultApi.StreamInfoListStreamIdPatch``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `StreamInfoListStreamIdPatch`: StreamInfoUpdateResponseType
    fmt.Fprintf(os.Stdout, "Response from `DefaultApi.StreamInfoListStreamIdPatch`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**streamId** | **string** | Identifies the stream for which the information is to be updated. | 

### Other Parameters

Other parameters are passed through a pointer to a apiStreamInfoListStreamIdPatchRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------

 **streamInfoToUpdateRequestType** | [**StreamInfoToUpdateRequestType**](StreamInfoToUpdateRequestType.md) |  | 

### Return type

[**StreamInfoUpdateResponseType**](StreamInfoUpdateResponseType.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

