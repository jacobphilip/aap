# StreamInfoListPostResponseType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StreamInfoListPosted** | Pointer to [**[]StreamInfoOutType**](StreamInfoOutType.md) |  | [optional] 

## Methods

### NewStreamInfoListPostResponseType

`func NewStreamInfoListPostResponseType() *StreamInfoListPostResponseType`

NewStreamInfoListPostResponseType instantiates a new StreamInfoListPostResponseType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStreamInfoListPostResponseTypeWithDefaults

`func NewStreamInfoListPostResponseTypeWithDefaults() *StreamInfoListPostResponseType`

NewStreamInfoListPostResponseTypeWithDefaults instantiates a new StreamInfoListPostResponseType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreamInfoListPosted

`func (o *StreamInfoListPostResponseType) GetStreamInfoListPosted() []StreamInfoOutType`

GetStreamInfoListPosted returns the StreamInfoListPosted field if non-nil, zero value otherwise.

### GetStreamInfoListPostedOk

`func (o *StreamInfoListPostResponseType) GetStreamInfoListPostedOk() (*[]StreamInfoOutType, bool)`

GetStreamInfoListPostedOk returns a tuple with the StreamInfoListPosted field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreamInfoListPosted

`func (o *StreamInfoListPostResponseType) SetStreamInfoListPosted(v []StreamInfoOutType)`

SetStreamInfoListPosted sets StreamInfoListPosted field to given value.

### HasStreamInfoListPosted

`func (o *StreamInfoListPostResponseType) HasStreamInfoListPosted() bool`

HasStreamInfoListPosted returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


