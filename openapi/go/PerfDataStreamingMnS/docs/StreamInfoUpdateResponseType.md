# StreamInfoUpdateResponseType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StreamInfoUpdated** | Pointer to [**StreamInfoOutType**](StreamInfoOutType.md) |  | [optional] 

## Methods

### NewStreamInfoUpdateResponseType

`func NewStreamInfoUpdateResponseType() *StreamInfoUpdateResponseType`

NewStreamInfoUpdateResponseType instantiates a new StreamInfoUpdateResponseType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewStreamInfoUpdateResponseTypeWithDefaults

`func NewStreamInfoUpdateResponseTypeWithDefaults() *StreamInfoUpdateResponseType`

NewStreamInfoUpdateResponseTypeWithDefaults instantiates a new StreamInfoUpdateResponseType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStreamInfoUpdated

`func (o *StreamInfoUpdateResponseType) GetStreamInfoUpdated() StreamInfoOutType`

GetStreamInfoUpdated returns the StreamInfoUpdated field if non-nil, zero value otherwise.

### GetStreamInfoUpdatedOk

`func (o *StreamInfoUpdateResponseType) GetStreamInfoUpdatedOk() (*StreamInfoOutType, bool)`

GetStreamInfoUpdatedOk returns a tuple with the StreamInfoUpdated field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStreamInfoUpdated

`func (o *StreamInfoUpdateResponseType) SetStreamInfoUpdated(v StreamInfoOutType)`

SetStreamInfoUpdated sets StreamInfoUpdated field to given value.

### HasStreamInfoUpdated

`func (o *StreamInfoUpdateResponseType) HasStreamInfoUpdated() bool`

HasStreamInfoUpdated returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


