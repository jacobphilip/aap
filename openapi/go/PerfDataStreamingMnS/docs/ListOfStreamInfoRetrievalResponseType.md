# ListOfStreamInfoRetrievalResponseType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ListOfStreamInfoOut** | Pointer to [**[]StreamInfoOutType**](StreamInfoOutType.md) |  | [optional] 

## Methods

### NewListOfStreamInfoRetrievalResponseType

`func NewListOfStreamInfoRetrievalResponseType() *ListOfStreamInfoRetrievalResponseType`

NewListOfStreamInfoRetrievalResponseType instantiates a new ListOfStreamInfoRetrievalResponseType object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListOfStreamInfoRetrievalResponseTypeWithDefaults

`func NewListOfStreamInfoRetrievalResponseTypeWithDefaults() *ListOfStreamInfoRetrievalResponseType`

NewListOfStreamInfoRetrievalResponseTypeWithDefaults instantiates a new ListOfStreamInfoRetrievalResponseType object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetListOfStreamInfoOut

`func (o *ListOfStreamInfoRetrievalResponseType) GetListOfStreamInfoOut() []StreamInfoOutType`

GetListOfStreamInfoOut returns the ListOfStreamInfoOut field if non-nil, zero value otherwise.

### GetListOfStreamInfoOutOk

`func (o *ListOfStreamInfoRetrievalResponseType) GetListOfStreamInfoOutOk() (*[]StreamInfoOutType, bool)`

GetListOfStreamInfoOutOk returns a tuple with the ListOfStreamInfoOut field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetListOfStreamInfoOut

`func (o *ListOfStreamInfoRetrievalResponseType) SetListOfStreamInfoOut(v []StreamInfoOutType)`

SetListOfStreamInfoOut sets ListOfStreamInfoOut field to given value.

### HasListOfStreamInfoOut

`func (o *ListOfStreamInfoRetrievalResponseType) HasListOfStreamInfoOut() bool`

HasListOfStreamInfoOut returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


