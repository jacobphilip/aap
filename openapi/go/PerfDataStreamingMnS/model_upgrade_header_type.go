/*
 * TS 28.550 Performance Data Streaming Service
 *
 * OAS 3.0.1 specification of the Performance Data Streaming Service
 *
 * API version: 16.3.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
	"fmt"
)

// UpgradeHeaderType the model 'UpgradeHeaderType'
type UpgradeHeaderType string

// List of Upgrade-HeaderType
const (
	WEBSOCKET UpgradeHeaderType = "websocket"
)

var allowedUpgradeHeaderTypeEnumValues = []UpgradeHeaderType{
	"websocket",
}

func (v *UpgradeHeaderType) UnmarshalJSON(src []byte) error {
	var value string
	err := json.Unmarshal(src, &value)
	if err != nil {
		return err
	}
	enumTypeValue := UpgradeHeaderType(value)
	for _, existing := range allowedUpgradeHeaderTypeEnumValues {
		if existing == enumTypeValue {
			*v = enumTypeValue
			return nil
		}
	}

	return fmt.Errorf("%+v is not a valid UpgradeHeaderType", value)
}

// NewUpgradeHeaderTypeFromValue returns a pointer to a valid UpgradeHeaderType
// for the value passed as argument, or an error if the value passed is not allowed by the enum
func NewUpgradeHeaderTypeFromValue(v string) (*UpgradeHeaderType, error) {
	ev := UpgradeHeaderType(v)
	if ev.IsValid() {
		return &ev, nil
	} else {
		return nil, fmt.Errorf("invalid value '%v' for UpgradeHeaderType: valid values are %v", v, allowedUpgradeHeaderTypeEnumValues)
	}
}

// IsValid return true if the value is valid for the enum, false otherwise
func (v UpgradeHeaderType) IsValid() bool {
	for _, existing := range allowedUpgradeHeaderTypeEnumValues {
		if existing == v {
			return true
		}
	}
	return false
}

// Ptr returns reference to Upgrade-HeaderType value
func (v UpgradeHeaderType) Ptr() *UpgradeHeaderType {
	return &v
}

type NullableUpgradeHeaderType struct {
	value *UpgradeHeaderType
	isSet bool
}

func (v NullableUpgradeHeaderType) Get() *UpgradeHeaderType {
	return v.value
}

func (v *NullableUpgradeHeaderType) Set(val *UpgradeHeaderType) {
	v.value = val
	v.isSet = true
}

func (v NullableUpgradeHeaderType) IsSet() bool {
	return v.isSet
}

func (v *NullableUpgradeHeaderType) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableUpgradeHeaderType(val *UpgradeHeaderType) *NullableUpgradeHeaderType {
	return &NullableUpgradeHeaderType{value: val, isSet: true}
}

func (v NullableUpgradeHeaderType) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableUpgradeHeaderType) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}

