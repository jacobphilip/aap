/*
 * TS 28.550 Performance Data Streaming Service
 *
 * OAS 3.0.1 specification of the Performance Data Streaming Service
 *
 * API version: 16.3.0
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package openapi

import (
	"encoding/json"
)

// ListOfStreamInfoUpdateResponseType struct for ListOfStreamInfoUpdateResponseType
type ListOfStreamInfoUpdateResponseType struct {
	ListOfStreamInfoUpdated *[]StreamInfoOutType `json:"listOfStreamInfoUpdated,omitempty"`
}

// NewListOfStreamInfoUpdateResponseType instantiates a new ListOfStreamInfoUpdateResponseType object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewListOfStreamInfoUpdateResponseType() *ListOfStreamInfoUpdateResponseType {
	this := ListOfStreamInfoUpdateResponseType{}
	return &this
}

// NewListOfStreamInfoUpdateResponseTypeWithDefaults instantiates a new ListOfStreamInfoUpdateResponseType object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewListOfStreamInfoUpdateResponseTypeWithDefaults() *ListOfStreamInfoUpdateResponseType {
	this := ListOfStreamInfoUpdateResponseType{}
	return &this
}

// GetListOfStreamInfoUpdated returns the ListOfStreamInfoUpdated field value if set, zero value otherwise.
func (o *ListOfStreamInfoUpdateResponseType) GetListOfStreamInfoUpdated() []StreamInfoOutType {
	if o == nil || o.ListOfStreamInfoUpdated == nil {
		var ret []StreamInfoOutType
		return ret
	}
	return *o.ListOfStreamInfoUpdated
}

// GetListOfStreamInfoUpdatedOk returns a tuple with the ListOfStreamInfoUpdated field value if set, nil otherwise
// and a boolean to check if the value has been set.
func (o *ListOfStreamInfoUpdateResponseType) GetListOfStreamInfoUpdatedOk() (*[]StreamInfoOutType, bool) {
	if o == nil || o.ListOfStreamInfoUpdated == nil {
		return nil, false
	}
	return o.ListOfStreamInfoUpdated, true
}

// HasListOfStreamInfoUpdated returns a boolean if a field has been set.
func (o *ListOfStreamInfoUpdateResponseType) HasListOfStreamInfoUpdated() bool {
	if o != nil && o.ListOfStreamInfoUpdated != nil {
		return true
	}

	return false
}

// SetListOfStreamInfoUpdated gets a reference to the given []StreamInfoOutType and assigns it to the ListOfStreamInfoUpdated field.
func (o *ListOfStreamInfoUpdateResponseType) SetListOfStreamInfoUpdated(v []StreamInfoOutType) {
	o.ListOfStreamInfoUpdated = &v
}

func (o ListOfStreamInfoUpdateResponseType) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.ListOfStreamInfoUpdated != nil {
		toSerialize["listOfStreamInfoUpdated"] = o.ListOfStreamInfoUpdated
	}
	return json.Marshal(toSerialize)
}

type NullableListOfStreamInfoUpdateResponseType struct {
	value *ListOfStreamInfoUpdateResponseType
	isSet bool
}

func (v NullableListOfStreamInfoUpdateResponseType) Get() *ListOfStreamInfoUpdateResponseType {
	return v.value
}

func (v *NullableListOfStreamInfoUpdateResponseType) Set(val *ListOfStreamInfoUpdateResponseType) {
	v.value = val
	v.isSet = true
}

func (v NullableListOfStreamInfoUpdateResponseType) IsSet() bool {
	return v.isSet
}

func (v *NullableListOfStreamInfoUpdateResponseType) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableListOfStreamInfoUpdateResponseType(val *ListOfStreamInfoUpdateResponseType) *NullableListOfStreamInfoUpdateResponseType {
	return &NullableListOfStreamInfoUpdateResponseType{value: val, isSet: true}
}

func (v NullableListOfStreamInfoUpdateResponseType) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableListOfStreamInfoUpdateResponseType) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}


